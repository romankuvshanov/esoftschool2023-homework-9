import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import AuthPageComponent from "./components/pages/AuthPageComponent/AuthPageComponent";
import RatingPageComponent from "./components/pages/RatingPageComponent/RatingPageComponent";
import GamePageComponent from "./components/pages/GamePageComponent/GamePageComponent";
import ActivePlayersPageComponent from "./components/pages/ActivePlayersPageComponent/ActivePlayersPageComponent";
import GamesHistoryPageComponent from "./components/pages/GamesHistoryPageComponent/GamesHistoryPageComponent";
import UsersListPageComponent from "./components/pages/UsersListPageComponent/UsersListPageComponent";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={"/"} element={<AuthPageComponent />}></Route>
        <Route path={"/rating"} element={<RatingPageComponent />}></Route>
        <Route path={"/game"} element={<GamePageComponent />}></Route>
        <Route
          path={"/activePlayers"}
          element={<ActivePlayersPageComponent />}
        ></Route>
        <Route
          path={"/gamesHistory"}
          element={<GamesHistoryPageComponent />}
        ></Route>
        <Route
          path={"/usersList"}
          element={<UsersListPageComponent />}
        ></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
