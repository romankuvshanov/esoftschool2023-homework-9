import "./BlockUnblockButton.scss";
import block from "./block.svg";

export default function BlockUnblockButton({isBlocked = false}) {
  return (
    <button className={"block-unblock-button"}>
      {isBlocked ? (
        "Разблокировать"
      ) : (
        <>
          <img src={block} alt={"Block sign icon"}></img> Заблокировать
        </>
      )}
    </button>
  );
}