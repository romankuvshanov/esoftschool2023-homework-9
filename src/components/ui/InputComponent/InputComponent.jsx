import "./InputComponent.scss";

export default function InputComponent({
  isWrong = false,
  isWrongMessage,
  type = "text",
  placeholder,
  value,
  onChange,
  onCopy,
  onKeyDown,
}) {
  return (
    <>
      <input
        className={`input-component ${isWrong && "input-component--error"}`}
        type={type}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onCopy={onCopy}
        onKeyDown={onKeyDown}
      />
      {isWrong && (
        <p className={"input-component__error-message"}>{isWrongMessage}</p>
      )}
    </>
  );
}
