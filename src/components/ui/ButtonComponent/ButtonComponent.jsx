import "./ButtonComponent.scss";
export default function ButtonComponent({
  children,
  inactive = false,
  type = "button",
  widthFull = false,
  color = "green",
  className = "",
  onClick
}) {
  return (
    <button
      className={`button-component ${
        inactive && "button-component--inactive"
      } ${widthFull && "button-component--width-full"} button-component--${color} ${className}`}
      type={type}
      disabled={inactive}
      onClick={onClick}
    >
      {children}
    </button>
  );
}
