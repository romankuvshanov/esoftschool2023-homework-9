import "./MessageContainer.scss";

export default function MessageContainer({ message }) {
  // TODO: Вопрос оставить в этой папке или перенести в папку "GamePageComponent" где этот компонент используется?
  return (
    <div className={`message-container message-container--${message.type}`}>
      <div className="message-container__msg-header">
        <div
          className={`message-container__subject-name message-container__subject-name--${
            message.type === "other" ? "x" : "zero"
          }`}
        >
          {message.from}
        </div>
        <div className="msg-header__time">{message.time}</div>
      </div>
      <div className="msg-header__msg-body">{message.text}</div>
    </div>
  );
}
