import "./StatusSign.scss";

export default function StatusSign({children, color = 'green'}) {
  return <span className={`status-sign status-sign--${color}`}>{children}</span>;
}