import "./CellContainer.scss";
import x_xxl from "./xxl-x.svg";
import zero_xxl from "./xxl-zero.svg";

export default function CellContainer({ onClick, cell, hasWon = false }) {
  return (
    <div
      className={`game-board__cell ${
        hasWon &&
        (cell === "x"
          ? "game-board__cell-green-won"
          : cell === "o" && "game-board__cell-red-won")
      }`}
      onClick={onClick}
    >
      {cell === "x" && (
        <img className={"game-board__img"} alt={"Big x icon"} src={x_xxl} />
      )}
      {cell === "o" && (
        <img className={"game-board__img"} alt={"Big o icon"} src={zero_xxl} />
      )}
    </div>
  );
}
