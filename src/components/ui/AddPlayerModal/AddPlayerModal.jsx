import "./AddPlayerModal.scss";
import ButtonComponent from "../ButtonComponent/ButtonComponent";
import closeIcon from "./Vector.svg";
import female from "./female.png";
import male from "./male.png";
import InputComponent from "../InputComponent/InputComponent";

export default function AddPlayerModal({ onClose }) {
  return (
    <div
      className={"add-player-modal-container"}
      onClick={(e) => {
        onClose();
      }}
    >
      <div className={"add-player-modal"} onClick={(e) => e.stopPropagation()}>
        <button className={"add-player-modal__close-button"} onClick={onClose}>
          <img src={closeIcon} alt={"X close icon button"} />
        </button>
        <p className={"add-player-modal__paragraph"}>Добавьте игрока</p>
        <label className={"add-player-modal__label"}>ФИО</label>
        <InputComponent
          type={"text"}
          placeholder={"Иванов Иван Иванович"}
        ></InputComponent>
        <div className={'add-player-modal__age-sex-container'}>
          <div>
            <label className={"add-player-modal__label"}>Возраст</label>
            <input type={'number'} id={'age'} className={'add-player-modal__age-input'} min={0} max={130}/>
          </div>
          <div>
            <label className={"add-player-modal__label"}>Пол</label>
            <input type={"radio"} id={"female-radio-button"} name={"sex"} hidden={true} defaultChecked={true} />
            <label htmlFor={"female-radio-button"} className={"sex-label"}>
              <img src={female} alt={"Female icon"} width={32} height={32} />
            </label>
            <input type={"radio"} id={"male-radio-button"} name={"sex"} hidden={true} />
            <label htmlFor={"male-radio-button"} className={"sex-label"}>
              <img src={male} alt={"Male icon"} width={32} height={32} />
            </label>
          </div>


        </div>
        <ButtonComponent widthFull={true}>Добавить</ButtonComponent>
      </div>
    </div>
  );
}
