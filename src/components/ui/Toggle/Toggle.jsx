import "./Toggle.scss";

export default function Toggle() {
  return (
    <div className={"toggle"}>
      <label htmlFor={"only-available-checkbox"}>Только свободные </label>
      <label className="switch" htmlFor={"only-available-checkbox"}>
        <input id={"only-available-checkbox"} type="checkbox" />
        <span className="slider round"></span>
      </label>
    </div>
  );
}