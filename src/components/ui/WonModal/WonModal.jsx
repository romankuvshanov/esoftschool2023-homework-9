import "./WonModal.scss";
import cup from "./cup.png";
import ButtonComponent from "../ButtonComponent/ButtonComponent";
import { useNavigate } from "react-router-dom";

export default function WonModal({ onHandleNewGame }) {
  const navigate = useNavigate();

  return (
    <div
      className={"won-modal-container"}
      onClick={(e) => {
        onHandleNewGame();
      }}
    >
      <div className={"won-modal"} onClick={(e) => e.stopPropagation()}>
        <img src={cup} alt={"Cup icon"} width={132} height={165} />
        <p className={'won-modal__paragraph'}>Владлен Пупкин победил!</p>
        <ButtonComponent onClick={onHandleNewGame} widthFull={true} className={'won-modal__new-game-button'}>Новая игра</ButtonComponent>
        <ButtonComponent onClick={() => navigate("/activePlayers")} widthFull={true} color={'grey'}>
          Выйти в меню
        </ButtonComponent>
      </div>
    </div>
  );
}
