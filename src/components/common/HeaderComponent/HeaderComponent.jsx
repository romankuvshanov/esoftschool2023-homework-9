import signOutIcon from "./signout-icon.svg";
import logo from "./s-logo.svg";
import "./HeaderComponent.scss";
import { Link } from "react-router-dom";
export default function HeaderComponent({ currentActiveTabNum }) {
  return (
    <header className={"header"}>
      <div>
        <img src={logo} alt={'App logo. Consists of "xoxo" string'} />
      </div>
      <div className={"header__nav-panel"}>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 0 && "header__nav-panel__menu-elem--active"
          }`}
        >
          <Link className={"header__nav-panel__menu-elem__link"} to={"/game"}>
            Игровое поле
          </Link>
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 1 && "header__nav-panel__menu-elem--active"
          }`}
        >
          <Link className={"header__nav-panel__menu-elem__link"} to={"/rating"}>
            Рейтинг
          </Link>
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 2 && "header__nav-panel__menu-elem--active"
          }`}
        >
          <Link
            className={"header__nav-panel__menu-elem__link"}
            to={"/activePlayers"}
          >
            Активные игроки
          </Link>
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 3 && "header__nav-panel__menu-elem--active"
          }`}
        >
          <Link
            className={"header__nav-panel__menu-elem__link"}
            to={"/gamesHistory"}
          >
            История игр
          </Link>
        </div>
        <div
          className={`header__nav-panel__menu-elem ${
            currentActiveTabNum === 4 && "header__nav-panel__menu-elem--active"
          }`}
        >
          <Link
            className={"header__nav-panel__menu-elem__link"}
            to={"/usersList"}
          >
            Список игроков
          </Link>
        </div>
      </div>
      <button className={"header__sign-out-button"}>
        <Link to={"/"}>
          <img src={signOutIcon} alt={"Sign-out icon"} />
        </Link>
      </button>
    </header>
  );
}
