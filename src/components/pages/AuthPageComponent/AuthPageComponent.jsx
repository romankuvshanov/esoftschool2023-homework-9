import dogImage from "./dog.png";
import "./AuthPageComponent.scss";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import InputComponent from "../../ui/InputComponent/InputComponent";
import ButtonComponent from "../../ui/ButtonComponent/ButtonComponent";
export default function AuthPageComponent() {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");
  const [loginWrong, setLoginWrong] = useState(null);
  const navigate = useNavigate();
  function handleKeyDown(e) {
    // Не даем пользователю ввести символы кроме: английские буквы, цифры и знаки: точка, нижнее подчеркивание
    const charCode = e.key.charCodeAt(0);

    if (
      charCode !== 46 && // .
      charCode !== 95 && // _
      !(charCode >= 48 && charCode <= 57) && // 0-9
      !(charCode >= 65 && charCode <= 90) && // A-Z
      !(charCode >= 97 && charCode <= 122) // a-z
    )
      e.preventDefault();
  }

  function handleSubmit(e) {
    e.preventDefault();
    console.log(`Логин: ${login}, пароль: ${password}`);

    if (login === "admin" && password === "admin") {
      navigate("/activePlayers");
    } else {
      setLoginWrong(true);
    }
  }

  return (
    <div className={"auth-form-container"}>
      <form className={"auth-form"} onSubmit={(e) => handleSubmit(e)}>
        <img
          src={dogImage}
          alt={"Изображение мультяшной собаки."}
          className={"auth-form__dog-img"}
        />
        <h1 className={"auth-form__headline"}>Войдите в игру</h1>
        <InputComponent
          isWrong={loginWrong}
          isWrongMessage={"Неверный логин"}
          type={"text"}
          placeholder={"Логин"}
          value={login}
          onChange={(e) => {
            setLogin(e.target.value);
            setLoginWrong(false);
          }}
          onCopy={(e) => e.preventDefault()}
          onKeyDown={(e) => handleKeyDown(e)}
        />
        <InputComponent
          isWrong={loginWrong}
          isWrongMessage={"Неверный пароль"}
          type={"password"}
          placeholder={"Пароль"}
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
            setLoginWrong(false);
          }}
          onCopy={(e) => e.preventDefault()}
          onKeyDown={(e) => handleKeyDown(e)}
        />

        <ButtonComponent className={'auth-form__button'} type={"submit"} inactive={!(login && password)} widthFull={true}>
          Войти
        </ButtonComponent>
      </form>
    </div>
  );
}
