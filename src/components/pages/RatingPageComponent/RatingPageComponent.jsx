import HeaderComponent from "../../common/HeaderComponent/HeaderComponent";
import {RATING_DATA} from "../../../constants/constants";
import "./RatingPageComponent.scss";

export default function RatingPageComponent() {
  return (
    <>
      <HeaderComponent currentActiveTabNum={1}></HeaderComponent>
      <section className={"rating-section"}>
        <h1 className={"rating-section__headline"}>Рейтинг игроков</h1>
        <table className={"rating-section__table"}>
          <thead>
            <tr>
              <th className={"table__table-head-cell"}>ФИО</th>
              <th className={"table__table-head-cell"}>Всего игр</th>
              <th className={"table__table-head-cell"}>Победы</th>
              <th className={"table__table-head-cell"}>Проигрыши</th>
              <th className={"table__table-head-cell"}>Процент побед</th>
            </tr>
          </thead>
          <tbody>
            {RATING_DATA.map((user) => {
              return (
                <tr key={user.id} className={"table__table-row"}>
                  <td className={"table__table-data-cell"}>{user.name}</td>
                  <td className={"table__table-data-cell"}>
                    {user.totalAmountOfGames}
                  </td>
                  <td className={"table__table-data-cell table__games-won"}>
                    {user.gamesWonAmount}
                  </td>
                  <td className={"table__table-data-cell table__games-lost"}>
                    {user.gamesLostAmount}
                  </td>
                  <td className={"table__table-data-cell"}>
                    {user.winsPercentage}%
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
