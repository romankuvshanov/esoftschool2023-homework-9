import HeaderComponent from "../../common/HeaderComponent/HeaderComponent";
import { GAMES_HISTORY } from "../../../constants/constants";
import "./GamesHistoryPageComponent.scss";
import zero from "./zero.svg";
import x from "./x.svg";
import cup from "./cup.png";

export default function GamesHistoryPageComponent() {
  return (
    <>
      <HeaderComponent currentActiveTabNum={3}></HeaderComponent>
      <section className={"games-history-section"}>
        <h1 className={"games-history-section__headline"}>История игр</h1>
        <table className={"games-history-section__table"}>
          <thead>
            <tr>
              <th className={"table__table-head-cell"}>Игроки</th>
              <th className={"table__table-head-cell"}></th>
              <th className={"table__table-head-cell"}></th>
              <th className={"table__table-head-cell"}>Дата</th>
              <th className={"table__table-head-cell"}>Время игры</th>
            </tr>
          </thead>
          <tbody>
            {GAMES_HISTORY.map((game) => {
              return (
                <tr key={game?.gameId} className={"table__table-row"}>
                  <td className={"table__table-data-cell-player"}>
                    <img
                      src={zero}
                      alt={"Zero icon"}
                      className={"table-data-cell__img"}
                    />{" "}
                    {game?.player1}{" "}
                    {game?.hasPlayer1Won && (
                      <img src={cup} alt={"Cup icon"} className={"table-data-cell__img"} width={24} height={24} />
                    )}
                  </td>
                  <td className={"table__table-data-cell"}>
                    <b className={'table-data-cell__bold-sign'}>против</b>
                  </td>
                  <td className={"table__table-data-cell-player"}>
                    <img
                      alt={"X icon"}
                      src={x}
                      className={"table-data-cell__img"}
                    />{" "}
                    {game?.player2}{" "}
                    {!game?.hasPlayer1Won && (
                      <img src={cup} alt={"Cup icon"} className={"table-data-cell__img"} width={24} height={24} />
                    )}
                  </td>
                  <td className={"table__table-data-cell"}>{game?.date}</td>
                  <td className={"table__table-data-cell"}>
                    {game?.gameDuration}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
// TODO: Разбить всё на компоненты избавиться от копипасты
