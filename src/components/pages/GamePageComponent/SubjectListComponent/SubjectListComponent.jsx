import "./SubjectListComponent.scss";
import zero from "../zero.svg";
import x from "../x.svg";

export default function SubjectListComponent() {
  return (
    <div className="subject-list-component">
      <h1 className="subject-list-component__headline">Игроки</h1>
      <div className="subject-list-component__container">
        <div className="subject-list-component__subject-container">
          <div>
            <img
              className="subject-list-component__subject-icon"
              alt={"Zero icon"}
              src={zero}
            />
          </div>
          <div className="subject-list-component__subject-info">
            <div className="subject-container__subject-name">
              Пупкин Владелен Игоревич
            </div>
            <div className="subject-container__subject-percent">63% побед</div>
          </div>
        </div>
        <div className="subject-list-component__subject-container">
          <div>
            <img
              className="subject-list-component__subject-icon"
              alt={"X icon"}
              src={x}
            />
          </div>
          <div className="subject-list-component__subject-info">
            <div className="subject-container__subject-name">
              Плюшкина Екатерина Викторовна
            </div>
            <div className="subject-container__subject-percent">23% побед</div>
          </div>
        </div>
      </div>
    </div>
  );
}