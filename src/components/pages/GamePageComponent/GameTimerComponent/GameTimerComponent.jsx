import "./GameTimerComponent.scss";
import { useEffect, useState } from "react";

export default function GameTimerComponent({stop = false, seconds, setSeconds}) {
  useEffect(() => {
    const interval = setInterval(() => {
      if (!stop) setSeconds((seconds) => seconds + 1);
    }, 1000);
    return () => clearInterval(interval);
  }, [stop]);

  return (
    <div className="game-timer-component">
      {Math.floor(seconds / 60)
        .toString()
        .padStart(2, "0")}
      :{(seconds % 60).toString().padStart(2, "0")}
    </div>
  );
}
