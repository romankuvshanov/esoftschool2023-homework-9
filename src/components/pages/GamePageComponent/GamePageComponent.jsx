import "./GamePageComponent.scss";
import sendButton from "./send-btn.svg";
import { MESSAGES } from "../../../constants/constants";
import { GAME_FIELD_INITIAL } from "../../../constants/constants";
import HeaderComponent from "../../common/HeaderComponent/HeaderComponent";
import { useState } from "react";
import CellContainer from "../../ui/CellContainer/CellContainer";
import MessageContainer from "../../ui/MessageContainer/MessageContainer";
import InputComponent from "../../ui/InputComponent/InputComponent";
import GameStepComponent from "./GameStepComponent/GameStepComponent";
import SubjectListComponent from "./SubjectListComponent/SubjectListComponent";
import GameTimerComponent from "./GameTimerComponent/GameTimerComponent";
import { getWinnerCellsIds, hasWinner } from "../../../common/common";
import WonModal from "../../ui/WonModal/WonModal";

export default function GamePageComponent() {
  const [currentPlayer, setCurrentPlayer] = useState("x");
  const [gameField, setGameField] = useState(GAME_FIELD_INITIAL);
  const [seconds, setSeconds] = useState(0);

  function handleCellClick(index) {
    // Если уже есть победитель, то ничего не делаем
    if (hasWinner(gameField)) return;

    const rowNumber = Math.floor(index / 3);
    const columnNumber = index % 3;

    if (gameField[rowNumber][columnNumber] === null) {
      // Обновляем gameField state не изменяя массив, а создавая новый
      const nextGameField = structuredClone(gameField);
      nextGameField[rowNumber][columnNumber] = currentPlayer;
      setGameField(nextGameField);

      setCurrentPlayer(currentPlayer === "x" ? "o" : "x");
    }
  }

  function resetTheGame() {
    setGameField(GAME_FIELD_INITIAL);
    setSeconds(0);
  }

  return (
    <>
    {hasWinner(gameField) && <WonModal onHandleNewGame={() => resetTheGame()} />}
      <HeaderComponent currentActiveTabNum={0}></HeaderComponent>
      <div className="main-container">
        <SubjectListComponent />
        <div className="game-container">
          <GameTimerComponent stop={hasWinner(gameField)} seconds={seconds} setSeconds={setSeconds} />
          <div className="game-container__game-board">
            {gameField.flat().map((cell, index) => {
              return (
                <CellContainer
                  cell={cell}
                  key={index}
                  onClick={() => handleCellClick(index)}
                  hasWon={getWinnerCellsIds(gameField).includes(index)}
                />
              );
            })}
          </div>
          <GameStepComponent currentPlayer={currentPlayer} />
        </div>
        <div className="chat-container">
          <div className="chat-container__msgs-container">
            {MESSAGES.map((message) => {
              return <MessageContainer message={message} key={message.id} />;
            })}
          </div>
          <div className="chat-container__msg-interactive-elements">
            <InputComponent type={"text"} placeholder={"Сообщение..."} />
            <button className={"msg-interactive-elements__button"}>
              <img alt={"Send button icon"} src={sendButton} />
            </button>
          </div>
        </div>
      </div>
    </>
  );
}
