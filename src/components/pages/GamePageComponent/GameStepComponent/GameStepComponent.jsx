import "./GameStepComponent.scss";
import x from "./x.svg";
import zero from "./zero.svg";

export default function GameStepComponent({currentPlayer}) {
  return (
    <div className="game-step-component">
      Ходит&nbsp;
      {currentPlayer === "x" ? (
        <>
          <img alt={"X icon"} src={x} />
          &nbsp;Плюшкина Екатерина
        </>
      ) : (
        <>
          <img alt={"Zero icon"} src={zero} />
          &nbsp;Владелен Пупкин
        </>
      )}
    </div>
  );
}