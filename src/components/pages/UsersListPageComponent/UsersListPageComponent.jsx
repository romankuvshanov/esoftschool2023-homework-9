import HeaderComponent from "../../common/HeaderComponent/HeaderComponent";
import { USERS_LIST } from "../../../constants/constants";
import "./UsersListPageComponent.scss";
import male from "./male.png";
import female from "./female.png";
import BlockUnblockButton from "../../ui/BlockUnblockButton/BlockUnblockButton";
import StatusSign from "../../ui/StatusSign/StatusSign";
import ButtonComponent from "../../ui/ButtonComponent/ButtonComponent";
import { useState } from "react";
import AddPlayerModal from "../../ui/AddPlayerModal/AddPlayerModal";

export default function UsersListPageComponent() {
  const [showAddPlayerModal, setShowAddPlayerModal] = useState(false);

  return (
    <>
      {showAddPlayerModal && <AddPlayerModal onClose={() => setShowAddPlayerModal(false)} />}
      <HeaderComponent currentActiveTabNum={4}></HeaderComponent>
      <section className={"users-list-section"}>
        <div className={"users-list-section__headline-container"}>
          <h1 className={"users-list-section__headline"}>Список игроков</h1>
          <ButtonComponent type={"button"} color={"green"} onClick={() => setShowAddPlayerModal(!showAddPlayerModal)}>
            Добавить игрока
          </ButtonComponent>
        </div>

        <table className={"users-list-section__table"}>
          <thead>
            <tr>
              <th className={"table__table-head-cell"}>ФИО</th>
              <th className={"table__table-head-cell"}>Возраст</th>
              <th className={"table__table-head-cell"}>Пол</th>
              <th className={"table__table-head-cell"}>Статус</th>
              <th className={"table__table-head-cell"}>Создан</th>
              <th className={"table__table-head-cell"}>Изменен</th>
              <th className={"table__table-head-cell"}></th>
            </tr>
          </thead>
          <tbody>
            {USERS_LIST.map((user) => {
              return (
                <tr key={user.id} className={"table__table-row"}>
                  <td className={"table__table-data-cell"}>{user?.name}</td>
                  <td className={"table__table-data-cell"}>{user?.age}</td>
                  <td className={"table__table-data-cell"}>
                    {user?.sex ? (
                      <img
                        src={male}
                        alt={"Male emoji icon"}
                        width={24}
                        height={24}
                      />
                    ) : (
                      <img
                        src={female}
                        alt={"Female emoji icon"}
                        width={24}
                        height={24}
                      />
                    )}
                  </td>
                  <td className={"table__table-data-cell"}>
                    {user.isBlocked ? (
                      <StatusSign color={"red"}>Заблокирован</StatusSign>
                    ) : (
                      <StatusSign color={"green"}>Свободен</StatusSign>
                    )}
                  </td>
                  <td className={"table__table-data-cell"}>{user?.created}</td>
                  <td className={"table__table-data-cell"}>{user?.changed}</td>
                  <td className={"table__table-data-cell"}>
                    <BlockUnblockButton isBlocked={user.isBlocked} />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
