import HeaderComponent from "../../common/HeaderComponent/HeaderComponent";
import { ACTIVE_USERS } from "../../../constants/constants";
import "./ActivePlayersPageComponent.scss";
import StatusSign from "../../ui/StatusSign/StatusSign";
import Toggle from "../../ui/Toggle/Toggle";
import ButtonComponent from "../../ui/ButtonComponent/ButtonComponent";

export default function ActivePlayersPageComponent() {
  return (
    <>
      <HeaderComponent currentActiveTabNum={2}></HeaderComponent>
      <section className={"active-players-section"}>
        <div className={"active-players-section__toggle"}>
          <h1 className={"active-players-section__headline"}>
            Активные игроки
          </h1>
          <Toggle />
        </div>

        <table className={"active-players-section__table"}>
          <tbody>
            {ACTIVE_USERS.map((user) => {
              return (
                <tr
                  key={user.id}
                  className={"active-players-section__table__table-row"}
                >
                  <td className={"table__table-data-cell"}>{user.name}</td>
                  {/* TODO: Вопрос: как лучше здесь сделать conditional rendering*/}
                  <td className={"table__table-data-cell"}>
                    {user.isFree ? (
                      <StatusSign color={"green"}>Свободен</StatusSign>
                    ) : (
                      user.isPlaying && (
                        <StatusSign color={"blue"}>В игре</StatusSign>
                      )
                    )}
                  </td>
                  <td className={"table__table-data-cell"}>
                    <ButtonComponent
                      type={"button"}
                      color={user.isFree ? "green" : "grey"}
                    >
                      Позвать играть
                    </ButtonComponent>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </section>
    </>
  );
}
